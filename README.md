<a name="readme-top"></a>
<!-- ABOUT THE PROJECT -->
# Jenkins agent for building docker images. 

This image is a Jenkins agent for building docker images. The agent runs as a standalone container and uses TCP or WebSockets to establish inbound connection to the Jenkins controller. 
This agent is powered by the Jenkins Remoting library, which version is being taken from [docker-in-bound-agent](https://github.com/jenkinsci/docker-inbound-agent).  

It is assumed that you have configured a node on your Jenkins controller which will enable this agent to establish connection. If not, kindly follow the section titled **JNLP Connection** in this [guide](https://docs.cloudbees.com/docs/cloudbees-ci-kb/latest/client-and-managed-masters/how-to-create-permanent-agents-with-docker) to configure a node on your Jenkins controller. 

> #### &#9888; Do not use in production. This is for development purposes only. 

<hr>

<!-- GETTING STARTED -->
## Running
**Steps to run the agent**

1. Clone this repo.
    ```sh
    git clone https://gitlab.com/vixen24/docker-agent.git
    ```

2. Build docker image.
    ```sh
    docker build -t docker-agent:1.0 .
    ```

3.  Start the service. Docker need root privileged to run. 
    ```sh
    docker run --init --privileged -d docker-agent:1.0 -url http://jenkins-server:port <secret> <agent name>
    ```

4.  Ensure container is running. Copy container ID.
    ```sh
    docker ps
    ```

5. Enter the below command to open the terminal of the container. 
    ```sh
    docker exec -it <container ID> bash
    ```

6.  Start docker from terminal. Optionally, you can start and stop docker from your Jenkinsfile with the same command.
    ```sh
    service docker start
    ```
    
7.  Exit the terminal.
    ```sh
    exit
    ```
8. Assuming your Jenkins node is configured properly, docker-agent will establish connection with Jenkins controller. You can now build your project. 

9.  Stop docker. 
    ```sh
    docker exec -it <container ID> bash
    service docker stop
    ```

